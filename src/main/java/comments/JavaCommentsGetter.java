package comments;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JavaCommentsGetter {
    String filePath;

    JavaCommentsGetter(String fp) {
        filePath = fp;
    }

    public String getJavaComments() {
        char commentChar;
        char ch;
        StringBuilder comment = new StringBuilder();
        try (FileReader fileReader = new java.io.FileReader(new File(filePath))) {
            while (fileReader.ready()) {
                ch = (char) fileReader.read();
                if (ch == '/' && (char) fileReader.read() == '/') {
                    comment.append("//");
                    while (true) {
                        commentChar = (char) fileReader.read();
                        if (commentChar == '\n') {
                            break;
                        }
                        comment.append(commentChar);
                    }
                    comment.append("\n");
                } else if (ch == '/' && (char) fileReader.read() == '*') {
                    comment.append("/*");
                    while (true) {
                        commentChar = (char) fileReader.read();
                        if (commentChar == '/') {
                            System.out.print(commentChar);
                            break;
                        }
                        comment.append(commentChar);
                    }
                    comment.append("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return comment.toString();
    }
}

