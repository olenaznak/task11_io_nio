package serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class App {
    public static void main(String[] args) {

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("src\\main\\resources\\gun.txt")))
        {
            TTC ttc = new TTC("Middle", 500, true, true);
            Gun g = new Gun(1, "Gun", 2, "USA", ttc, "Steel");
            oos.writeObject(g);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("src\\main\\resources\\gun.txt")))
        {
            Gun g=(Gun)ois.readObject();
            System.out.println(g.toString());
        }
        catch(Exception ex){

            System.out.println(ex.getMessage());
        }
    }
}
