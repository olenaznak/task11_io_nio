package serialization;

import java.io.Serializable;

public class Gun implements Serializable {
    private String model;
    private int handy;
    private String origin;
    private transient TTC ttc;
    private String material;
    private int id;

    public Gun(int id, String model, int handy, String origin, TTC ttc, String material) {
        this.id = id;
        this.model = model;
        this.handy = handy;
        this.origin = origin;
        this.ttc = ttc;
        this.material = material;
    }

    public Gun() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getHandy() {
        return handy;
    }

    public void setHandy(int handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public TTC getTtc() {
        return ttc;
    }

    public void setTtc(TTC ttc) {
        this.ttc = ttc;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "serialization.Gun{" +
                "model='" + model + '\'' +
                ", handy=" + handy +
                ", origin='" + origin + '\'' +
                ", ttc=" + ttc +
                ", material='" + material + '\'' +
                '}';
    }
}
