package serialization;

public class TTC {
    private String range;
    private int sightingRange;
    private Boolean hasClip;
    private Boolean hasOpticalSight;

    public TTC(String range, int sightingRange, Boolean hasClip, Boolean hasOpticalSight) {
        this.range = range;
        this.sightingRange = sightingRange;
        this.hasClip = hasClip;
        this.hasOpticalSight = hasOpticalSight;
    }

    public TTC() {
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public int getSightingRange() {
        return sightingRange;
    }

    public void setSightingRange(int sightingRange) {
        this.sightingRange = sightingRange;
    }

    public Boolean getHasClip() {
        return hasClip;
    }

    public void setHasClip(Boolean hasClip) {
        this.hasClip = hasClip;
    }

    public Boolean getHasOpticalSight() {
        return hasOpticalSight;
    }

    public void setHasOpticalSight(Boolean hasOpticalSight) {
        this.hasOpticalSight = hasOpticalSight;
    }

    @Override
    public String toString() {
        return "serialization.TTC{" +
                "range='" + range + '\'' +
                ", sightingRange=" + sightingRange +
                ", hasClip=" + hasClip +
                ", hasOpticalSight=" + hasOpticalSight +
                '}';
    }
}
