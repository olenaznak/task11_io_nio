package comparing;

import java.io.*;
import java.util.Date;

public class Comparator {

    public static void main(String[] args) {
        long start;
        long time;
        System.out.println("Start of reading without buffer");
        try {
            InputStream is = new FileInputStream("src\\main\\resources\\Java.pdf");
            start = new Date().getTime();
            while (is.read() > 0) {
            }
            time = new Date().getTime() - start;
            System.out.println("End of reading without buffer " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Start of reading with buffer");
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream("src\\main\\resources\\Java.pdf"));
            start = new Date().getTime();
            while (bis.read() > 0) {
            }
            time = new Date().getTime() - start;
            System.out.println("End of reading with buffer " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
