package directorytree;

import java.io.File;

public class DirectoryTree {

    public void printTree(String fiePath) {
        File root = new File(fiePath);
        printTreeRecursively(root);
    }

    private void printTreeRecursively(File f) {
        if(!f.isDirectory()) {
            System.out.println("   " + f.getName());
            return;
        }
        System.out.println(f.getName());
        String[] names = f.list();
        if(names == null) {
            return;
        }

        File[] files = f.listFiles();
        for (int i = 0; i < files.length; i++) {
            printTreeRecursively(files[i]);
        }
    }
}
